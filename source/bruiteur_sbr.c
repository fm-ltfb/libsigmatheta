/*   bruiteur.c                     F. Vernotte - First release: 1994/02/02 */
/*					    Sigma-Theta version: 2015/10/26 */
/*                                    Modified by Attila Kinali: 2017/06/11 */
/*                              Integration into library by BD - 2023/07/10 */
/*   Simulation of time error x(t) and/or frequency deviation yk samples    */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or � or Copr. Universit� de Franche-Comt�, Besan�on, France    */
/* Contributor: Fran�ois Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <sys/sysinfo.h>
#include "filtre.h"
#include "sigma_theta.h"

/**
 * 
*/
int bruiteur(double *x, double ***fip, unsigned long nbr_dat, char rep, double tau0, filter_coef fcoeff)
{
	double kdb, adjsig;  //, dN;
	size_t i;
	unsigned long nbr_stat=0, debut, size_mem;
	struct sysinfo sinfo;

	assert((rep<48)||(rep>50));

	// get sysinfo to determine memory size
    if(sysinfo(&sinfo))
    {
        printf("sysinfo failed, something is wrong, seriously wrong!\n");
        exit(1);
    }
	/* For generating a sequence of N samples with a low cut-off frequency much lower than 1/(N tau0),
 	we generate a sequence of M samples; 
  	we keep only a subsequence of N consecutive data (the beginning is randomly chosen). 
	The M-N other data are sent to the trash! */
    if (nbr_dat<4) nbr_dat=4;
    /* ensure that the allocated data is smaller than half of total memory
    in order to avoid heavy swapping */
    if (nbr_dat*(32+8)*2 > sinfo.totalram)
	{
        printf("\n\nError: Requested number of samples would require to store data more than\nhalf the total RAM size\n");
        printf("Aborting to prevent the system from comming to a grinding halt\n");
        exit(1);
    }
    nbr_dat=(unsigned long)ceil(log((double)nbr_dat)/log((double)2));
    nbr_dat=(unsigned long)pow((double)2,(double)nbr_dat);
    if (nbr_dat>65536) size_mem=nbr_dat;
    else size_mem=(long)65536;

	if (nbr_stat<4) nbr_stat=4;
    if (nbr_stat>nbr_dat) nbr_stat=nbr_dat;
   	//dN=(double)nbr_stat;  // TODO : remove??

	/* The high cut-off frequency is not arbitrarily set at the Nyquist frequency */
	//hp2*=tau0;
	kdb=((double)(nbr_dat-nbr_stat))/((double)RAND_MAX); /* Maximum value of the beginning index. */
	adjsig=stu_gausseq(nbr_dat,0); /* Generation of a unity white noise sequence. */
	
	/// Initialisation de GR dans gausseg (FV) qui est ensuite utilisé dans filtreur (FV)...

    stu_filtreur(nbr_dat,x,tau0,fcoeff); /* Filtering of the sequence. */
    if (nbr_dat==nbr_stat) debut = 0;
    else debut = (unsigned long) (rand()*kdb); /* Setting of the beginning of the subsequence. */

	if (rep=='2')
	{
		fip = (double***)malloc(size_mem * sizeof(3*sizeof(int*)));
	}
	else
	{
		fip = (double***)malloc(size_mem * sizeof(2*sizeof(int*)));
	}
    if (fip==NULL)
    {
       	printf("Not enough memory for %ld data\n",nbr_dat);
       	exit(-1);
    }

	// 1st column
	for(i=0;i<nbr_stat;++i)
	{
		*fip[i][0] = (double)(debut+i)*tau0;
	}
	// 2nd column
	if (rep=='1' || rep=='2')
	{
		for(i=0;i<nbr_stat;++i)
		{
			*fip[i][1] = x[debut+i];
		}
	}
	else
	{
		stu_yk_xt(nbr_dat,x,0,tau0);
		for(i=0;i<nbr_stat;++i)
		{
			*fip[i][1] = x[debut+i];
		}
	}
	// 3rd column (only if rep==2)
	if (rep=='2')
	{
		stu_yk_xt(nbr_dat,x,0,tau0);
		for(i=0;i<nbr_stat;++i)
		{
			*fip[i][2] = x[debut+i];
		}
	}
	
	//free(x);

	return 0;
}










